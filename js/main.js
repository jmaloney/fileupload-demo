var vube = {};
vube.upload = (function() {
	var
		config = {
			url: "upload.php",
			dataType: 'json',
			submit: function (e, data) {
				console.log('submit');
				var id;
				id = createRecord();
				data.formData = {id:id};
				$('.holder').append('<div>' + id + '</div>');
				console.log(data);
			},
			progress: function (e, data) {
				
			},
			progressall: function (e, data) {

			},
			done: function (e, data) {
				console.log('done');
				console.log(data);
			},
			fail: function (e, data) {
				console.log('fail');
				console.log(data);
			}
		},
		createRecord = function() {
			var id;
			$.ajax({
				url: "mock.php",
				dataType: 'json',
				async: false,
				success: function(json) {
					id = json.id;
				}
			});
			return id;
		}
	;
	return {
		config: config
	}
})();

$(document).ready(function() {
	    $('#files').fileupload(vube.upload.config);
});


